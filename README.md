##Coeficiente de correlação de Pearson[1]
Em estatística descritiva, o coeficiente de correlação de Pearson, mede o grau da correlação (e a direcção dessa correlação - se positiva ou negativa) entre duas variáveis de escala métrica (intervalar ou de rácio/razão).

Este coeficiente, normalmente representado por ρ ou r assume apenas valores entre -1 e 1.

##Tipos de correlação[3]

Correlação negativa r < Zero, quanto mais próximo de -1 maior é a correlação negativa.

Sem correlação linear r = Zero, quanto mais próximo de 0, menor é a correlação linear.

Correlação positiva r > Zero, quanto mais próximo de 1, maior é a correlação positiva. 


##A correlação linear em relação ao valor de r[3]

Entre -1 e 0: A correlação pode ser negativa forte (quando tende mais para -1) ou negativa fraca (quando tende para 0);

Entre 0 e +1: A correlação pode ser positiva forte (quando tende para +1) ou positiva fraca (quando tende para 0);

No pono 0 a correlação não exite.

##Interpretando Coeficiente de correlação de Pearson[2]

0.9 para mais ou para menos indica uma correlação muito forte.

0.7 a 0.9 positivo ou negativo indica uma correlação forte.

0.5 a 0.7 positivo ou negativo indica uma correlação moderada.

0.3 a 0.5 positivo ou negativo indica uma correlação fraca.

0 a 0.3 positivo ou negativo indica uma correlação desprezível.

##Referência

[1]Coeficiente de correlação de Pearson, wikipedia, https://pt.wikipedia.org/wiki/Coeficiente_de_correla%C3%A7%C3%A3o_de_Pearson

[2]Hinkle DE, Wiersma W, Jurs SG. Applied Statistics for the Behavioral Sciences. 5th ed. Boston: Houghton Mifflin; 2003.

[3]Correlação e Regressão Linear - Tratamento e Análise de Dados e Informações - EACH/USP, youtube, https://www.youtube.com/watch?v=dWZ7oBiarsA