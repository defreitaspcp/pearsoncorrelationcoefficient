/*
Author : de Freitas, P.C.P
Description - Pearson Correlation Coefficient
*/
#include<iostream>
#include<cmath>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double var(double data[], int length)
{
	double sum = 0;
	for (int i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum;
}
double cov(double a[], double b[], int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		sum += (a[i] - mean(a, length))*(b[i] - mean(b, length));
	}
	return sum;
}
double PearsonCorrelationCoefficient(double a[], double b[], int length)
{
	return (cov(a, b, length)) / (sqrt(var(a,length)*var(b,length)));
}
int main()
{
	double CDAM20181[24] = { 1.49,1.09,0.96,1.22,1.24,1.32,1.21,0.99,0.96,1.06,1.02,1.05,
		1.02,0.96,1.18,0.99,0.98,1.86,0.99,1.15,1.21,1.08,1.06,0.96 };
	double CDEAM20181[24] = { 1.94726,1.46045,1.46045,0,1.55781,0,1.94726,1.16836,1.65517,0,1.60649,
		1.26572,0.973631,1.65517,0,1.07099,0,1.75254,1.65517,1.75254,0,0,1.07099,0 };
	cout << PearsonCorrelationCoefficient(CDAM20181, CDEAM20181, 24) << endl;
	cout << endl;

	double CDAM20182[8] = { 1.02,0.99,0.84,1.06,1.02,1.02,0.99,0.99 };
	double CDEAM20182[8] = { 1.07177,1.26316,1.22488,1.33971,0.995215,0.344498,1.07177,0.688995 };
	cout << PearsonCorrelationCoefficient(CDAM20182, CDEAM20182, 8) << endl;
	cout << endl;

	double CDES20181[11] = { 1.18,1.15,1.09,1.09,1.09,1.08,1.05,1.02,1.02,0.99,0.99 };
	double CDEES20181[11] = { 0,0,1.85393,0,1.97753,0,1.11236,1.85393,0.865169,1.60674,1.73034 };
	cout << PearsonCorrelationCoefficient(CDES20181, CDEES20181,11) << endl;
	cout << endl;

	double CDAAM20181[11] = { 1.86,1.49,1.21,1.24,0.99,1.02,0.96,0.96,1.09,0.96,1.15 };
	double CDAEAM20181[11] = { 1.04485,1.16095,1.16095,0.92876,0.986807,0.957784,
		0.986807,0.986807,0.870712,0.870712,1.04485 };
	cout << PearsonCorrelationCoefficient(CDAAM20181, CDAEAM20181, 11) << endl;
	cout << endl;

	double CDAAM20182[5] = { 1.06,1.02,0.99,0.84,0.99 };
	double CDAEAM20182[5] = { 1.12179,0.897436,0.897436,1.02564,1.05769 };
	cout << PearsonCorrelationCoefficient(CDAAM20182, CDAEAM20182, 5) << endl;
	cout << endl;

	double CDAES20181[4] = { 1.09,1.09,1.02,0.99 };
	double CDAEES20181[4] = { 1.06667,1,1,0.933333 };
	cout << PearsonCorrelationCoefficient(CDAES20181, CDAEES20181, 4) << endl;
	cout << endl;

	return 0;
}